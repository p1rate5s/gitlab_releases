# Project Title

Gitlab_Releases is a project created to demonstrate how to create Gitlab Releases.  It consists of a perl script that is used to gather info from Gitlab and use that info to call the Gitlab API call for create_release.

## Getting Started

Clone this repo to your Gitlab project and update the necessary variables present in the gitlab-ci.yml in your Gitlab secrets.

### Prerequisites

In the gitlab-ci.yml file you will need to cpan install the following modules for the script to work:

```
      - cpan install REST::Client
      - cpan install JSON
```



## Deployment

This runs solely on a Gitlab runner with a container that contains the perl bits.  It is suggested to build a new perl container that contains JSON and REST so that you save time in the downloads from CPAN.


## Authors

* **Richard Murphy** - *Initial work* - [Sophron.io/Demo/gitlab_releases](https://gitlab.sophron.io/demo/gitlab_releases)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


## Screenshot

![alt text](/Screenshot_20200217_145650.png "Screenshot")
