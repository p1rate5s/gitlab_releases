use REST::Client;
use JSON;
use Data::Dumper;

my $api_url = $ARGV[0];
my $project_id = $ARGV[1];
my $milestone = $ARGV[2];
my $pipe_url = $ARGV[3];
my $project_web_url = $ARGV[4];
my $gitlab_token = $ARGV[5];
my $release_url = "$api_url/projects/$project_id/releases";
my $project_url = "$api_url/projects/$project_id";

## Create variable, treated as a file handle for printing description of the release.
my $release_description = "";
open my $fhrd, ">", \$release_description or die $OS_ERROR;

## Temp testing constants
#$milestone = 'v1.0';
#$project_url = "https://gitlab.sophron.io/api/v4/projects/114";
#$release_url = "https://gitlab.sophron.io/api/v4/projects/114/releases";
#$pipe_url = "$project_url/pipelines/1193";

## Set up the http headers
my $client = REST::Client->new();
$client->addHeader('Content-Type', 'application/json');
$client->addHeader('charset', 'UTF-8');
$client->addHeader('Accept', 'application/json');
$client->addHeader('PRIVATE-TOKEN', $gitlab_token);

## Get all the milestones
$client->GET("$project_url/milestones");
my $json = $client->responseContent();
my @decoded_json = decode_json( $client->responseContent() );
print Dumper @decoded_issues;  ## uncomment to debug

## count number of milestones
my $count = scalar @{$decoded_json[0]};

## find the milestone we want and get the issues associated
foreach my $i (0..$count-1){
        my $title = $decoded_json[0][$i]{title};
        if ( $title eq $milestone ){
                $mile_id = $decoded_json[0][$i]{id};
                ## Print milestone to the release descr
                print $fhrd "## Milestone $milestone  \n";
                ## get all of the issues in this milestone
                $client->GET("$project_url/milestones/$mile_id/issues");
                my $ijson = $client->responseContent();
                my @decoded_issues = decode_json($client->responseContent());
                print Dumper @decoded_issues;  ## uncomment to debug
                
                ## print issues to release description
                print $fhrd "### Changelog  \n";
                my $count2 = scalar @{$decoded_issues[0]};  ## count the issues
                ## loop thru all the issues in the milestone and print them
                foreach my $i (0..$count2-1){
                        print $fhrd " * $decoded_issues[0][$i]{title} - $decoded_issues[0][$i]{web_url}  \n";               
                }
        }
}
## Print the current pipeline status to the release page
print $fhrd "### Pipeline results  \n";
print $fhrd qq\[![pipeline status]($project_web_url/badges/master/pipeline.svg)]($pipe_url)\;

## build the json in a perl hash for the 'create release' api call
%rel = (
        "name", "Release $milestone",
        "tag_name", "$milestone",
        "ref", "master",
        "description", "$release_description",
        "milestones", ["$milestone"],
         );

##  encode the json from the perl hash
$req = encode_json \%rel;
##  post to gitlab thus creating the release with all of the stuff we printed above
$client->POST($release_url, $req);

## print to the screen/the job console the output of our post
print $client->responseContent();
close $fhrd;

